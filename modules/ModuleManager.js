const { fork } = require('child_process');
const { EventEmitter } = require('stream');
const { join } = require('path'); 
const process = require('process');
const path = require('path');

const ModulePaths = {
    web: join(process.cwd(), '/modules/web/web.js')
}

module.exports = class ModuleManager extends EventEmitter{
    constructor(modules){
        super(EventEmitter);

        if(!Array.isArray(modules)){
            console.error('Module Manager expected type array');
            throw TypeError;
        }
        console.log('Starting Modules');
        
        this.ModuleForks = {};
        //Module Event Handler: 
        modules.forEach(m => {
            if(m in ModulePaths){ 
                this.ModuleForks[m] = fork(ModulePaths[m]);
                this.ModuleForks[m].on('message', m => {
                    if(!m.hasOwnProperty('CMD') && m.hasOwnProperty('MCSID')){
                        console.warn("Ditched Message from Parent: ", m);
                        return;
                    }
                
                    switch(m.CMD){
                        case 'START':
                           this.emit('START', (m.MCSID));
                            break;
                        case 'STOP':
                           this.emit('STOP', (m.MCSID));
                            break;
                    }
                }); 
                
                console.log('Started Module: ', m);
            } else {
                throw 'Invalid Module Start Attempted';
            }
            
        });


        this.on('MCSSTART', (MCSID) => {
            for (let f in this.ModuleForks){
                this.ModuleForks[f].send({CMD: 'MCSSTART', 'MCSID': MCSID});
            }
        });
    }

}