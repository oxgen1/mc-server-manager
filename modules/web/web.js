const { readFile } = require('fs'); 
const net = require('net');
const { createServer } = require("http");


const { Server } = require('socket.io');
const { Tail } = require("tail");
const path = require('path');

const mc_servers = require(path.join(process.env.PWD, '/conf/servers.json')).servers;

const PID = process.argv[1];
const consoleLogsPath = '/tmp';
console.log(process.env.PWD + '/modules/web/public/index.html');

const httpServer = createServer(function(req, res){
    if (req.url === '/mc'){
        console.log(process.env.PWD + '/modules/web/public/index.html');
        readFile(process.env.PWD + '/modules/web/public/index.html', (err, data) => {
            res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
            res.write(data);
            res.end(); 
        });
    } else if (req.url === '/css'){
        readFile(process.env.PWD +  '/modules/web/public/stylesheet.css', (err, data) => {
            res.writeHead(200, {'Content-Type': 'text/css','Content-Length':data.length});
            res.write(data);
            res.end(); 
        });
    } 
});
const io = new Server(httpServer, {});
setup_websocket();

httpServer.listen(3232);

var serverData = {};
var socketsToClear = [];
var serverString = "";


process.on('message', m => {
    if(!m.hasOwnProperty('CMD') && m.hasOwnProperty('MCSID')){
        console.warn("Ditched Message from Parent: ", m);
        return;
    }

    switch(m.CMD){
        case 'MCSSTART':

            var serverD = setup_server(m.MCSID);
            console.log('MCSSTART: Server: ')
             if(!serverData.hasOwnProperty(m.MCSID)){

                if(serverD.status.out){
                    serverD.tail.on('line', (line) => {
                        io.to(serverD.names.out).emit('console', line);
                    });
                }
                socketsToClear.push(serverD.names.in);

                if(serverString.indexOf(m.MCSID) ==- 1)
                    serverString = serverString.concat(m.MCSID, ';');
            }

            serverData[m.MCSID] = serverD;

            io.sockets.emit('reattach');
    }
});


function setup_websocket(){
   
    var rooms = [];
    //var serverData = [];
    /*for (const s of servers) {
        serverData[s.id] = setup_server(s);
        if(serverData[s.id].status.out){
            serverData[s.id].tail.on('line', (line) => {
                io.to(serverData[s.id].names.out).emit('console', line);
            });
        }
        socketsToClear.push(serverData[s.id].names.in);

    }*/

    io.on('connection', (socket) => {
        //TODO We need to update this string.
        serverString = mc_servers.map(e => e.id).join(';');

        socket.on('START', (mcid) => {
            process.send({CMD: 'START', MCSID: mcid});
        });
        socket.on('STOP', (mcid) => {
            process.send({CMD: 'STOP', MCSID: mcid});
        }); 

        socket.emit('serverList', serverString);
        console.log("serverString: ", serverString);
        rooms[socket.id] = [];
        socket.on('attach', (server) => {
            socketsToClear.forEach(s => socket.removeAllListeners(s));
            console.log("Sockets to Clear: ", socketsToClear);
            console.log("Rooms to clear: ", rooms[socket.id]);
            console.log("Socket Actual Rooms: ",socket.rooms);
            for(var v of rooms[socket.id]){
                socket.leave(v);
            }
            rooms[socket.id] = [];
            console.log('client attached to server: ', server);
            if(typeof serverData[server] !== 'undefined'){
                let serverD = serverData[server];
                //console.log(serverD);
                if(serverD.status.out){
                    socket.join(serverD.names.out);
                    rooms[socket.id].push(serverD.names.out);
                }
                socket.on(serverD.names.in, (cmd) =>{
                    console.log('command recived from client: ', serverD.id, cmd);
                    serverD.socket.write(`${cmd}\n`);

                });
                
            } else {
                console.log('Server not found');
            }
        })
    });

}

function setup_server(serverID){
    let roomName = {
        out: false,
        in: false
    }
        roomName.out = `${serverID}-out`;
        roomName.in = `${serverID}-in`;
    let status = {
        out: false,
        in: false
    }

    let serverState = {};
    let tail, socket  = false;
    try {
        //setup Out
        tail = setup_tails(serverID);
        //setup in
        socket = connect_to_socket(serverID)
    } catch (e) {
        console.error('Error Setting up Server IO: ', e);
    }
   
    
    if(tail) {
        status.out = true;
        serverState.tail = tail;
    } 

    if(socket){
        status.in = true;
        serverState.socket = socket;
    }
    serverState.id = serverID;
    serverState.status = status;
    serverState.names = roomName;
    return serverState;
}

function setup_tails(serverID){
    let logWatcher;
    try {
        logWatcher = new Tail(`${consoleLogsPath}/mc-${serverID}.out`);
        return logWatcher;
    } catch (e){
        console.error("Error finding Log Files for server: ", serverID, e);
        return false;
    }
}

function connect_to_socket(serverID){
    let socketConnection;
    try {
        socketConnection = net.connect(`/tmp/mc-${serverID}.sock`);
        console.log('WEB SERVER: Connecting to socket.')
        return socketConnection;
    } catch (e){
        console.error("Error Connecting to in socket for server: ", serverID, e);
        return false
    }
}