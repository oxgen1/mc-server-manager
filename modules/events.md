ModuleHandler Listens on Child Processes for {CMD: 'cmd', MCSID: 'minecraft server id'}
    CMDS:
        START -> emits start to listener on main process (index.js:220) which calls to start server, does not change stdio for minecraft server. 
        STOP -> emits stop to listner on main process which calls to stop server
        MCSSTART -> This is the intial server start up, which sets up stdio for the minecraft server.
    Listens to:
        MCSSTART -> which is emited my main process on the callback of open_streams (start_server->open_streams->callback).

MODULES:

WEB:
    Listens to:
        MCSSTART -> Triggers to listen on stdio. This is the inital Start, up