const config = require('./conf/config.json');
const servers = require('./conf/servers.json').servers;

const ModuleManager = require('./modules/ModuleManager');

const c = require("child_process");
const fs = require("fs")
const net = require('net');

const { Readable, Writable } = require("stream");

const { parallel, every, doUntil } = require("async");

const fetch = import('node-fetch');


var spawns = {};
var listeners = {};
var serverState = {};

var modules = new ModuleManager(['web']);
/* State Up Stuff */
function get_start_cmd(server){

    return {
        cmd: config.javaPath, 
        args: 
            [`-Xmx${server.max_mem}M`, 
            `-Xms${server.start_mem}M`, 
            '-XX:+UseG1GC',
            '-XX:+ParallelRefProcEnabled',
            '-XX:MaxGCPauseMillis=200',
            '-XX:+UnlockExperimentalVMOptions',
            '-XX:+DisableExplicitGC',
            '-XX:+AlwaysPreTouch',
            '-XX:G1NewSizePercent=30',
            '-XX:G1MaxNewSizePercent=40',
            '-XX:G1HeapRegionSize=8M',
            '-XX:G1ReservePercent=20',
            '-XX:G1HeapWastePercent=5',
            '-XX:G1MixedGCCountTarget=4',
            '-XX:InitiatingHeapOccupancyPercent=15',
            '-XX:G1MixedGCLiveThresholdPercent=90',
            '-XX:G1RSetUpdatingPauseTimePercent=5',
            '-XX:SurvivorRatio=32',
            '-XX:+PerfDisableSharedMem',
            '-XX:MaxTenuringThreshold=1',
            '-Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true -Djline.terminal=jline.UnsupportedTerminal',
            '-jar',
            `${server.jar_name}`,
            'nogui']};
}

function init_servers(){
    //console.log(config, servers);
    for(const s of servers){
        serverState[s.id] = {
            stopped: false,
            running: false
        };
        if(s.auto_start){
            start_server(s);
        }
    }
}

function start_server(server){
    console.log("Starting Server: ", server.id, config.path+server.id+'/');
    var so = get_start_cmd(server);
    try{

        open_streams(server, stdio => {

            let spawn = c.spawn(so.cmd, so.args, {cwd: config.path+server.id, stdio: stdio});
            create_listeners(server, spawn);
            
            spawns[server.id] = spawn;
            const listener = net.createServer({allowHalfOpen: false}, c => {
                console.log('client connected');
                c.on('end', () => {
                    c.unpipe(spawns[server.id].stdin);
                    c.end();
                    console.log('client disconnected');
                });
                c.write('hello\r\n');
                c.pipe(spawn.stdin, {end: false});
            }).listen(`/tmp/mc-${server.id}.sock`, () => {
                console.log(`Server Listening on: /tmp/mc-${server.id}.sock`);
            });
            listeners[server.id] = listener;

            //setTimeout(()=> {
            //}, 15000);
            serverState[server.id].running = true;
            modules.emit('MCSSTART', (server.id));
        });

        /*let stream = fs.createWriteStream('./streams/test');
        console.log(stream);
            */
        return true;
    } catch (e) {
        console.error('Error Starting Server: ' + server.id+ ' with error: '+ e);
        return false;
    }
}

function create_listeners(server, spawn){
    
        //Handle Autostart
        spawn.on("exit", (code) =>{
            console.log("This emitted before an attept at closing the listenrs")
            let listener = listeners[server.id];

            listener.close(((err) => { 
               console.log(err);
            }));

            if(server.auto_restart && !serverState[server.id].stopped){
                console.log(`Server ${server.id} exited with code ${code}, the server will be restarted in 5 seconds.`)
                setTimeout(() => {
                    start_server(server);
                }, 5000);
            }

            
        });
    
}

function open_streams(server, callback) {
    let stdio = [];

    //Create files streams for Fd 0 and 1
    parallel([
        function(callback){
            try{
                let stdoutWrite = fs.createWriteStream(`/tmp/mc-${server.id}.out`);
                callback(null, stdoutWrite);
            } catch(e) {
                callback(e, false);
            }
        }
    ], function(err, results) {
        //Need to ensure that stream.ready = true or steam.on('ready') has been called for both of these: ("xThe stream's underlying file descriptor is duplicated in the child process to the fd that corresponds to the index in the stdio array. The stream must have an underlying descriptor (file streams do not until the 'open' event has occurred)." See: https://nodejs.org/dist/latest-v16.x/docs/api/child_process.html#optionsstdio )
        
        if(!err && results){
            
            doUntil(
                function(callback){
                    //console.log(results);
                    every(results, 
                        function(stream, cb){
                            cb(err, !stream.pending);
                    }, function(err, result){
                        callback(err, result)
                    })

                },
                function(result, callback){
                    //console.log(result);
                    setTimeout(function() {
                        callback(null, result);
                    }, 1000);
                },
                function(err, result){
                    //console.log("test:",  results);
                    results.push(process.stderr);
                    const testWritable = new Writable({
                        write(chunk, encoding, next){
                            next();
                        }
                    });
                    
                    stdio[0] = 'pipe';
                    stdio[1] = results[0];
                    stdio[2] = process.stderr;
                    callback(stdio);
                })
        }
    });
}

function getServerFromID(serverID) {
    for (let s of servers){
        if(s.id === serverID) {
            return s;
        }
    }
    return false;
}


process.on('uncaughtExceptionMonitor', (err, origin) =>  {
    fs.writeSync(
        process.stderr.fd,
        `Caught exception: ${err}\n` +
        `Exception origin: ${origin}`
      );

    for(let s in spawns){
        console.log('Closing Server: \n')
        spawns[s].kill('SIGKILL');
    }

    //fetch('https://discord.com/api/webhooks/925937662941089812/f9Foi4qoBTTeiuW58KsNJTmEfGFgdhVe8IVbqkXTxl0Z0i-cMZZ3jwmjAYVjUUvc7Cwv', {method: 'POST', body: JSON.stringify({content: 'Program Exited'})});
});

process.on('exit', (code) => {
    for(let s in spawns){
        console.log('Closing Server:')
        spawns[s].kill('SIGKILL');
    }

    //fetch('https://discord.com/api/webhooks/925937662941089812/f9Foi4qoBTTeiuW58KsNJTmEfGFgdhVe8IVbqkXTxl0Z0i-cMZZ3jwmjAYVjUUvc7Cwv', {method: 'POST', body: JSON.stringify({content: 'Program Exited'})});

});

modules.on('START', (serverId) => {
    var server = getServerFromID(serverId);
    if(server){
        if(serverState[serverId].running === false){
            serverState[serverId].stopped = false;
            start_server(server);
        }
    }
});

modules.on('STOP', (serverId) => {
    var server = getServerFromID(serverId);
    if(server){
        serverState[serverId].stopped = true;
        spawns[serverId].kill('SIGTERM');
        serverState[serverId].running = false;

    }
});

init_servers();

setInterval(()=> {
    for(let ss in serverState){
        console.log(ss, serverState[ss]);
    }
}, 1000);